# alt-busy-state

A UI support library which facilitates the management and tracking of busy and loading states by monitoring RxJS observables.
