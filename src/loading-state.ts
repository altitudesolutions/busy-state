/**
 * Represents the loading state of a `BusyStateProvider`.
 */
export enum LoadingState {
    /**
     * The `BusyStateProvider` is in the initialisation state.
     */
    Initial,
    /**
     * The `BusyStateProvider` is actively loading.
     */
    Loading,
    /**
     * The `BusyStateProvider` has completed loading.
     */
    Loaded,
    /**
     * Loading failed.
     */
    Failed
}
