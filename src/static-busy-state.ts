import { immediate } from "@altitude/async";
import { Observable } from "rxjs";

import { IBusyStateProvider } from "./busy-state-provider";
import { LoadingState } from "./loading-state";
/**
 * A BusyStateProvider the state of which does not change.
 */
export class StaticBusyState implements IBusyStateProvider {
    /**
     * Emits `true` if the instance is managing one or more active worker actions, otherwise emits `false`.
     */
    readonly busyState$: Observable<boolean>;
    /**
     * Emits the current `LoadingState` of the instance.
     */
    readonly loadingState$: Observable<LoadingState>;

    constructor(
        /**
         * Returns the loading state of the instance.
         */
        readonly loadingState: LoadingState = LoadingState.Loaded,
        /**
         * Returns `true` if the instance is monitoring one or more active worker actions, otherwise returns `false`.
         */
        readonly isBusy: boolean = false
    ) {
        this.loadingState$ = immediate(loadingState);
        this.busyState$ = immediate(isBusy);
    }
}
