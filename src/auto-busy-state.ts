import * as Async from "@altitude/async";
import { Disposables } from "@altitude/disposables";
import * as ErrorMessage from "@altitude/error-message";
import { Semaphore } from "@altitude/semaphore";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { catchError } from "rxjs/operators";

import { IBusyStateProvider } from "./busy-state-provider";
import { LoadingState } from "./loading-state";
/**
 * A BusyStateProvider the state of which is controlled automatically.
 */
export class AutoBusyState implements IBusyStateProvider {
    /**
     * Emits the current `LoadingState` of the instance.
     */
    readonly loadingState$: Observable<LoadingState>;
    private _activeCount = 0;
    private readonly _busy = new Semaphore();
    private readonly _busyState$ = new BehaviorSubject<boolean>(false);
    private readonly _disposables: Disposables = new Disposables();
    private _disposed = false;
    private _error: Subject<any> | undefined;
    private _error$: Observable<any> | undefined;
    private _errors = false;
    private _isPreparing = false;
    private readonly _loadingState$ = new BehaviorSubject<LoadingState>(
        LoadingState.Initial
    );
    private _state: LoadingState = LoadingState.Initial;
    constructor() {
        this.loadingState$ = this._loadingState$.asObservable();
    }
    /**
     * Emits `true` if the instance is managing one or more active worker actions, otherwise emits `false`.
     */
    get busyState$(): Observable<boolean> {
        disposedGuard(this);

        return this._busy.state$;
    }
    /**
     * Emits any errors as they are raised by actions the instance is monitoring.
     */
    get error$(): Observable<any> {
        disposedGuard(this);

        if (this._error == undefined) {
            this._error = new Subject<any>();
            this._error$ = this._error.asObservable();
        }

        return <Observable<any>>this._error$;
    }
    /**
     * Returns `true` if the instance is monitoring one or more active worker actions, otherwise returns `false`.
     */
    get isBusy(): boolean {
        disposedGuard(this);

        return this._busy.isSet;
    }
    /**
     * Returns `true` if the instance has been disposed, otherwise returns `false`.
     */
    get isDisposed(): boolean {
        return this._disposed;
    }
    /**
     * Returns `true` if the instance is in the `LoadingState.Loaded` state, otherwise returns `false`.
     */
    get isLoaded(): boolean {
        disposedGuard(this);

        return this._state === LoadingState.Loaded;
    }
    /**
     * Returns `true` if the instance is in the `LoadingState.Loading` state, otherwise returns `false`.
     */
    get isLoading(): boolean {
        disposedGuard(this);

        return this._state === LoadingState.Loading;
    }
    /**
     * Returns `true` if the instance is in the preparing state (see the `enterPrepare` and `exitPrepare` methods), otherwise
     * returns `false`.
     */
    get isPreparing(): boolean {
        disposedGuard(this);

        return this._isPreparing;
    }
    /**
     * Returns the current `LoadingState` of the instance.
     */
    get loadingState(): LoadingState {
        disposedGuard(this);

        return this._state;
    }
    /**
     * Unsubscribes from any managed subscriptions and releases any resources associated with the instance.
     */
    dispose(): void {
        if (this._disposed) return;
        this._disposed = true;
        this._disposables.dispose();
        this._busy.dispose();
        Async.safeUnsubscribe(this._loadingState$);
        Async.safeUnsubscribe(this._busyState$);
        Async.safeUnsubscribe(this._error);
        this._errors = false;
        this._activeCount = 0;
        this._isPreparing = false;
    }
    /**
     * Cauases the instance to enter a preparation state, during which the `register` method may be invoked freely.
     * This method will throw if invoked while the instance is not in the `LoadingState.Initial` state (see also `exitPrepare`).
     */
    enterPrepare() {
        disposedGuard(this);
        if (this._isPreparing) {
            throw new Error(
                ErrorMessage.invalidOpDuringState("preparing", "AutoBusyState")
            );
        }
        if (this._state !== LoadingState.Initial) {
            throw new Error(
                ErrorMessage.invalidOpOutsideState("Initial", "AutoBusyState")
            );
        }
        this._isPreparing = true;
        this.updateState(LoadingState.Loading);
    }
    /**
     * Cause the instance to exit the preparation state. This method will throw if invoked while the instance is not
     * in the preparation state (see also `enterPrepare`).
     */
    exitPrepare() {
        disposedGuard(this);
        if (!this._isPreparing) {
            throw new Error(
                ErrorMessage.invalidOpOutsideState("preparing", "AutoBusyState")
            );
        }
        this._isPreparing = false;
        if (this._errors) {
            this.updateState(LoadingState.Failed);
        } else if (
            this._activeCount <= 0 &&
            this.loadingState === LoadingState.Loading
        ) {
            this.updateState(LoadingState.Loaded);
        }
    }
    /**
     * Registers an action with the instance, which will remain in a busy state until all such registered actions have emitted or
     * ore or more has failed.
     *
     * This method may be invoked at any time during the instance's lifecycle.
     * @param action The action to register.
     */
    registerBusy(action: Observable<any>): void {
        disposedGuard(this);
        if (action == undefined) {
            throw new Error(ErrorMessage.argumentNull("action"));
        }
        this._busy.monitor(
            action.pipe(
                catchError(e => {
                    this.raiseError(e);

                    return e;
                })
            )
        );
    }
    /**
     * Registers one or more observable loading actions with the instance, which will remain in a `LoadingState.Loading`
     * state until all such registered actions have emitted or one or more has failed.
     *
     * This method will throw if invoked while the instance is not in either the `LoadingState.Initial` or preparation state
     * (see the `enterPrepare` and `exitPrepare`).
     * @param actions The actions to register.
     */
    registerLoading(...actions: Observable<any>[]) {
        disposedGuard(this);
        if (!(this.isPreparing || this._state === LoadingState.Initial)) {
            throw new Error(
                ErrorMessage.invalidOpOutsideState(
                    "preparing or Initial",
                    "AutoBusyState"
                )
            );
        }
        if (!this._isPreparing) {
            try {
                this.enterPrepare();
                this.monitor(actions);
            } finally {
                this.exitPrepare();
            }
        } else {
            this.monitor(actions);
        }
    }
    /**
     * Resets the instance to the `LoadingState.Initial` state and detaches from any monitored loading and busy observables.
     */
    reset() {
        disposedGuard(this);
        this._disposables.dispose();
        this._busy.reset();
        this._errors = false;
        this._activeCount = 0;
        this._isPreparing = false;
        this.updateState(LoadingState.Initial);
    }
    private handleLoadingError(e: any) {
        if (!this._isPreparing && this.loadingState === LoadingState.Loading) {
            this.updateState(LoadingState.Failed);
        }
        this._errors = true;
        this.raiseError(e);
    }

    private monitor(actions: Observable<any>[]) {
        actions.forEach(() => {
            this._activeCount += 1;
        });
        actions.forEach(observable => {
            this._disposables.subscribeOnce(
                observable,
                () => {
                    this._activeCount -= 1;
                    if (this._activeCount <= 0) {
                        if (
                            !this.isPreparing &&
                            this.loadingState === LoadingState.Loading
                        ) {
                            this.updateState(LoadingState.Loaded);
                        }
                    }
                },
                error => {
                    this.handleLoadingError(error);
                }
            );
        });
    }

    private raiseError(e: any): void {
        if (this._error != undefined && !this.isDisposed) {
            this._error.next(e);
        }
    }

    private updateState(state: LoadingState) {
        if (this.isDisposed) return;

        if (this._state !== state) {
            this._state = state;
            // Queue the events so that any initiating client context that may have subscribed to them completes before they are raised.
            setTimeout(() => {
                if (!this._disposed) {
                    this._loadingState$.next(state);
                }
            }, 0);
        }
    }
}

function disposedGuard(t: AutoBusyState): void {
    if (t.isDisposed) throw new Error(ErrorMessage.objectDisposed());
}
