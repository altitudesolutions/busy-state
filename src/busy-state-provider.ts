import { Observable } from "rxjs";

import { LoadingState } from "./loading-state";
/**
 * An interface implemented by objects which report the aggregate state of asynchronous background operations
 * which affect the state of a view.
 */
export interface IBusyStateProvider {
    /**
     * An `Observable` representing the active busy state.
     */
    readonly busyState$: Observable<boolean>;
    /**
     * Indicates whether or not a background operation which inhibits a view's functionality is occurring.
     * Typically, a user's interaction with the view will be restricted for the duration of such an operation.
     */
    readonly isBusy: boolean;
    /**
     * Represents the state of aysnchronous operations that a view is dependent upon to fully construct its interface.
     */
    readonly loadingState: LoadingState;
    /**
     * An `Observable` representing the active loading state.
     */
    readonly loadingState$: Observable<LoadingState>;
}
