import * as Async from "@altitude/async";
import { BehaviorSubject, Observable } from "rxjs";

import { IBusyStateProvider } from "./busy-state-provider";
import { LoadingState } from "./loading-state";
/**
 * A BusyStateProvider the state of which is controlled manually.
 */
export class ManualBusyState implements IBusyStateProvider {
    /**
     * Gets or set the busy state.
     *
     * Returns `true` if the instance is monitoring one or more active worker actions, otherwise returns `false`.
     */
    get isBusy(): boolean {
        return this._busyState$.value;
    }
    set isBusy(value: boolean) {
        this._busyState$.next(value === true);
    }
    /**
     * Gets or set the loading state.
     */
    get loadingState(): LoadingState {
        return this._loadingState$.value;
    }

    set loadingState(value: LoadingState) {
        this._loadingState$.next(value);
    }
    /**
     * Emits `true` if the instance is managing one or more active worker actions, otherwise emits `false`.
     */
    readonly busyState$: Observable<boolean>;
    /**
     * Emits the current `LoadingState` of the instance.
     */
    readonly loadingState$: Observable<LoadingState>;
    private readonly _busyState$: BehaviorSubject<boolean>;
    private readonly _loadingState$: BehaviorSubject<LoadingState>;

    constructor(
        initialLoadingState: LoadingState = LoadingState.Loaded,
        initialBusyState: boolean = false
    ) {
        this._loadingState$ = new BehaviorSubject<LoadingState>(
            initialLoadingState
        );
        this._busyState$ = new BehaviorSubject<boolean>(initialBusyState);
        this.loadingState$ = this._loadingState$.asObservable();
        this.busyState$ = this._busyState$.asObservable();
    }
    /**
     * Releases any subscriptions associated with the instance.
     */
    dispose(): void {
        Async.safeUnsubscribe(this._loadingState$);
        Async.safeUnsubscribe(this._busyState$);
    }
}
