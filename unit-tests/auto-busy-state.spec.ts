import * as Async from "@altitude/async";
import { BehaviorSubject, Subject, throwError } from "rxjs";
import { filter, map } from "rxjs/operators";

import { AutoBusyState, LoadingState } from "../src";

// tslint:disable: no-magic-numbers

describe("busy-state.AutoBusyState", () => {
    it("Should emit busy at any time with no prepare (and therefore no loading)", () => {
        busyIterations(new AutoBusyState());
    });

    it("Should emit busy at any time after prepare (with no loading)", () => {
        const target = new AutoBusyState();
        target.enterPrepare();
        target.exitPrepare();
        busyIterations(target);
    });

    it("Should emit busy at any time during prepare (with no loading)", () => {
        const target = new AutoBusyState();

        target.enterPrepare();
        busyIterations(target);
        target.exitPrepare();
    });

    it("Should emit busy at any time during loading", async () => {
        const target = new AutoBusyState();
        target.enterPrepare();
        target.registerLoading(new Subject<void>());
        target.exitPrepare();
        await awaitNextLoadingState(target, LoadingState.Loading);
        busyIterations(target);
    });

    it("Should complete loading with no monitored observables", async () => {
        const target = new AutoBusyState();
        await awaitNextLoadingState(target, LoadingState.Initial);
        target.enterPrepare();
        await awaitNextLoadingState(target, LoadingState.Loading);
        target.exitPrepare();
        await awaitNextLoadingState(target, LoadingState.Loaded);
    });

    it("Should complete loading during preparation", async () => {
        const target = new AutoBusyState();
        await awaitNextLoadingState(target, LoadingState.Initial);
        target.enterPrepare();
        target.registerLoading(new BehaviorSubject(true));
        target.registerLoading(new BehaviorSubject(true));
        await awaitNextLoadingState(target, LoadingState.Loading);
        target.exitPrepare();
        await awaitNextLoadingState(target, LoadingState.Loaded);
    });

    it("Should complete loading aysnchronously after preparation", async () => {
        const target = new AutoBusyState();
        const trigger1 = new Subject<void>();
        const trigger2 = new Subject<void>();
        await awaitNextLoadingState(target, LoadingState.Initial);
        target.enterPrepare();
        target.registerLoading(trigger1);
        target.registerLoading(trigger2);
        await awaitNextLoadingState(target, LoadingState.Loading);
        target.exitPrepare();
        await awaitNextLoadingState(target, LoadingState.Loading);
        trigger2.next();
        await awaitNextLoadingState(target, LoadingState.Loading);
        trigger1.next();
        await awaitNextLoadingState(target, LoadingState.Loaded);
    });

    it("Should ignore false loading positives", async () => {
        const target = new AutoBusyState();
        const trigger1 = new Subject<void>();
        const trigger2 = new Subject<void>();
        await awaitNextLoadingState(target, LoadingState.Initial);
        target.enterPrepare();
        target.registerLoading(trigger1);
        target.registerLoading(trigger2);
        await awaitNextLoadingState(target, LoadingState.Loading);
        target.exitPrepare();
        await awaitNextLoadingState(target, LoadingState.Loading);
        trigger2.next();
        await awaitNextLoadingState(target, LoadingState.Loading);
        trigger2.next();
        await awaitNextLoadingState(target, LoadingState.Loading);
        trigger1.next();
        await awaitNextLoadingState(target, LoadingState.Loaded);
    });

    it("Should emit busy errors", () => {
        let error: string | undefined;
        const target = new AutoBusyState();
        target.error$.subscribe((e: string) => {
            error = e;
        });
        target.registerBusy(throwError("Failed"));
        expect(error).toEqual("Failed");
    });

    it("Should emit loading errors", () => {
        let error: string | undefined;
        const target = new AutoBusyState();
        target.error$.subscribe((e: string) => {
            error = e;
        });
        target.enterPrepare();
        target.registerLoading(throwError("Failed"));
        expect(error).toEqual("Failed");
        target.exitPrepare();
        expectLoadingState(target, LoadingState.Failed);
    });

    it("Should emit loading$ event with no monitored observables", async () => {
        const ls: LoadingState[] = [];

        const target = new AutoBusyState();
        target.loadingState$.subscribe(v => {
            ls.push(v);
        });
        target.enterPrepare();
        await awaitNextLoadingState(target, LoadingState.Loading);
        target.exitPrepare();
        await awaitLoadingState(target, LoadingState.Loaded);
        expect(ls).toEqual([
            LoadingState.Initial,
            LoadingState.Loading,
            LoadingState.Loaded
        ]);
    });

    it("Should emit loading$ event with registerLoading", async () => {
        const ls: LoadingState[] = [];
        const trigger = new Subject<void>();

        const target = new AutoBusyState();
        target.loadingState$.subscribe(v => {
            ls.push(v);
        });
        target.enterPrepare();
        target.registerLoading(trigger);
        target.exitPrepare();
        trigger.next();
        await awaitLoadingState(target, LoadingState.Loaded);
        expect(ls).toEqual([
            LoadingState.Initial,
            LoadingState.Loading,
            LoadingState.Loaded
        ]);
    });

    it("Should not emit loading$ when disposed", async () => {
        const ls: LoadingState[] = [];
        const trigger = new Subject<void>();

        const target = new AutoBusyState();
        target.loadingState$.subscribe(v => {
            ls.push(v);
        });
        target.enterPrepare();
        target.registerLoading(trigger);
        target.exitPrepare();
        // Surrender execution to allow LoadingState.Loading to emit
        await Async.awaitDelay(10).toPromise();
        target.dispose();
        trigger.next();
        // Surrender execution to ensure that LoadingState.Loaded does not emit
        await Async.awaitDelay(10).toPromise();
        expect(ls).toEqual([LoadingState.Initial, LoadingState.Loading]);
    });
});

function expectLoadingState(target: AutoBusyState, state: LoadingState): void {
    const text = enumName(LoadingState, state);
    expect(enumName(LoadingState, target.loadingState))
        .withContext(`Expected loading state to be "${text}"`)
        .toBe(text);
}

async function awaitLoadingState(target: AutoBusyState, state: LoadingState) {
    const text = enumName(LoadingState, state);
    const p = Async.observeOnce(
        target.loadingState$.pipe(
            filter(v => v === state),
            map(v => enumName(LoadingState, state))
        )
    ).toPromise();

    await p;

    expect(enumName(LoadingState, target.loadingState))
        .withContext(`Expected loading state to be "${text}"`)
        .toBe(text);

    // tslint:disable-next-line: no-floating-promises
    expectAsync(p).toBeResolvedTo(text);
}

async function awaitNextLoadingState(
    target: AutoBusyState,
    state: LoadingState
) {
    const text = enumName(LoadingState, state);
    const p = Async.observeOnce(target.loadingState$)
        .pipe(map(v => enumName(LoadingState, state)))
        .toPromise();

    await p;

    expect(enumName(LoadingState, target.loadingState))
        .withContext(`Expected loading state to be "${text}"`)
        .toBe(text);

    // tslint:disable-next-line: no-floating-promises
    expectAsync(p).toBeResolvedTo(text);
}

function busyIterations(target: AutoBusyState): void {
    const iterations = 3;
    const trigger = new Subject<void>();

    let busyState: "busy" | "not-busy" | undefined;
    target.busyState$.subscribe(v => {
        busyState = v ? "busy" : "not-busy";
    });

    let count = 0;
    while (count < iterations) {
        count += 1;
        target.registerBusy(trigger);
        expect(target.isBusy)
            .withContext("Expected it to be busy")
            .toBe(true);
        expect(busyState)
            .withContext("Expected it to have emitted busy")
            .toBe("busy");
        trigger.next();
        expect(target.isBusy)
            .withContext("Expected it to not be busy")
            .toBe(false);
        expect(busyState)
            .withContext("Expected it to have emitted not busy")
            .toBe("not-busy");
    }
    expect(count).toBe(iterations);
}

function enumName(enumType: any, enumValue: any): string | undefined {
    for (const n in LoadingState) {
        // tslint:disable-next-line: no-unsafe-any
        if (enumType[n] === enumValue) return n;
    }

    return undefined;
}
